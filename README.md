# Bouncy Ball

![screenshot](bounces.gif)

**Bouncy ball only supports Blender 2.7x**

This is a toy addon for that I wrote for Blender inspired by the [Bouncy Ball plasmoid](https://store.kde.org/p/1172489/). It's a little side project to help me learn how to work with the Bgl/Blf. I also wanted to see how far I could push Functional Programming in an addon (quite a bit all things considered).

I don't think I'll be working more on this since it's served its purpose. Feel free to fork!

## Things I didn't do (but you could)

- Fix the bounce count so bounces aren't added when the ball is rolling on the ground
- Stop gravity from affecting velocity when the ball is on the ground
- Make the ball rotate according to angular velocity
- Squash and stretch on bounces
- Have multiple balls
- Use gradients instead of solid colors
- Rewrite it however you like
